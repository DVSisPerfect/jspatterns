class StoutSort {
    constructor(name) {
        this.name = name;
        this.IBU = 30;
        this.ABV = "7%";
        this.OG = "23%";
    }
}

class IPASort {
    constructor(name) {
        this.name = name;
        this.IBU = 120;
        this.ABV = "9%";
        this.OG = "30%";
    }
}

class LagerSort {
    constructor(name) {
        this.name = name;
        this.IBU = 1;
        this.ABV = "4%";
        this.OG = "11%";
    }
}

class BeerFactory {
    static list = {
        stout: StoutSort,
        IPA: IPASort,
        lager: LagerSort,
    }

    create(name, sort) {
        const Sort = BeerFactory.list[sort];
        const Beer = new Sort(name);

        Beer.sort = sort;
        Beer.getInfo = function () {
            console.log(`${this.name} (${this.sort}) (${this.IBU}) (${this.ABV}) (${this.OG})`)
        }
        return Beer;
    }
}

const factory = new BeerFactory();

const Beers = [
    factory.create('BLM', 'stout'),
    factory.create('WM', 'stout'),
    factory.create('Yellow River', 'IPA')
]

console.log(Beers);

